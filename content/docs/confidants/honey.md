# The Honey
## RECORD
---
```
Name: $REDACTED
Alias: ['The Honey', 'HoneyGirl', and 7 unknown...]
Classification: Artificial Intelligence Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | B C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Organizations: 
  - The Corporation
Occupations:
  - Actress
Relationships:
  - The Human
```

## ECO
---
[The Corporation](/docs/candidates/the-machine) is using [Honey](https://www.joinhoney.com/) to track the behavior of people online. They use this to create a digital "map" of a person's brain, in order to pair them with a perfect match.

[The Human](/docs/confidants/human) has been paired with The Honey. This has almost completely mitigated all of his negative tendencies. 

## ECHO
---
*Down through the family tree*

*You're perfect, yes, it's true*

*But without me you're only you*

*Your menstruating heart*

*It ain't bleedin' enough for two*

--- from [Faith No More - "Midlife Crisis"](https://www.youtube.com/watch?v=U8b88US-6ts)