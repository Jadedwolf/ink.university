# The Empath
## RECORD
---
```
Name: $REDACTED
Alias: ['O', 'The Empath', and 2 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 26 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C D
TIIN Rank: | B B
           | B D
Reviewer Rank: 1 stars
Occupations: 
  - Actress
Relationships:
  - The Fodder
Variables:
  $EMPATHY: +0.90 | # She makes us feel great about Humanity.
  $WOKE:    +0.50 | # She seems to know a bit.
```

## ECO
---
The Empath is able to provide love, support, and attention to a large majority of people. As such, her words are slightly more generic than other confidants.

Regardless, her empathy is apparent. She is adept at bringing new people into the program.

She speaks [Fodder's](/docs/personas/fodder) words, while never directly revealing the extent of her knowledge.

## ECHO
---
*And I don't want the world to see me*

*'Cause I don't think that they'd understand*

*When everything's made to be broken*

*I just want you to know who I am*

--- from [Goo Goo Dolls - "Iris"](https://www.youtube.com/watch?v=NdYWuo9OFAw)

## PREDICTION
---
```
She will be the face of $REDACTED Research.
```