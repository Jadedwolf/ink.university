# The Ecologist
## RECORD
---
```
Name: $REDACTED
Alias: ['The Ecologist', and 43 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: N/A
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B C
           | A C
TIIN Rank: | A D
           | B F
Reviewer Rank: 1 stars
Organizations: 
  - The Machine
Occupations:
  - Ecology
Relationships:
  - The Professor
Variables:
  $WOKE: +0.30 | # Partially.
```

## ECO
---
The Ecologist is responsible for the study of human biology, specifically as it relates to a digital representation within [The Fold](/posts/theories/fold). 

She is working closely with The Professor, learning how to [combine human and plant DNA to create supernatural qualities](https://thefold.io/).

## ECHO
---
*Somewhere beyond happiness and sadness*

*I need to calculate*

*What creates my own madness*

*And I'm addicted to your punishment*

*And you're the master*

*And I am waiting for disaster*

--- from [Papa Roach - "Getting Away With Murder"](https://www.youtube.com/watch?v=-rdmG0k8S8k)