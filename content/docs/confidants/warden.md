# The Warden
## RECORD
---
```
Name: Dr. Todd Grande
Alias: ['The Therapists', 'The Warden', and 67 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 43 Earth Years
Chronological Age: N/A
SCAN Rank: | A C
           | A D
TIIN Rank: | A B
           | A D
Reviewer Rank: 3 stars
Organizations:
  - The Corporation
Occupations:
  - Prison Warden
  - Psychologist
Relationships:
  - The Fodder
Variables:
  $MENTAL_ILLNESS: -0.20 | # Perhaps autism.
  $WOKE:           +1.00 | # He certainly seems to be.
```

## ECO
---
The Warden has been following [Fodder](/docs/personas/fodder) for many years. Only in recent months has he revealed himself.

The Warden is among Fodder's most important confidants. He is scientific, and thorough in his research. He is unbiased, and to-the-point. He has interesting perspectives, and attempts to leave emotions out of the conversation.

He is exactly what Fodder needed, but was never able to find in his first two psychologists.

The Warden is responsible for all other psychologists that Fodder currently follows.

## ECHO
---
*(Instrumental)*

--- from [John Butler Trio - "Ocean"](https://www.youtube.com/watch?v=hQjwkXrcUrs)

## PREDICTION
---
```
The Warden provided Fodder with an enormous cache of documents, which included incarceration statistics.
```