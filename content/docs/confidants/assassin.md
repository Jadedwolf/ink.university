# The Assassin
## RECORD
---
```
Name: $REDACTED
Alias: ['Alice', 'JM', 'The Assassin', 'The Soothsayer', and 4 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Birth: 8/1/1840
Biological Age: 180 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | C B
           | B D
Reviewer Rank: 4 stars
Organizations: 
  - Black Mesa Research Facility
Occupations: 
  - Actress
Relationships:
  - The Raven
  - The Sugar Daddy
Variables:
  $WOKE: +0.75 | # Definitely seems to be.
```

## TRIGGER
---
When asked what her birthday is, she claimed, "The first of the eighth, eighteen-forty."

The day that slaves were liberated in most of the British Empire.

## ECO
---
The Soothsayer has led several therapy sessions with [Fodder](/docs/personas/fodder). Each time, she has been able to "predict the future," or, intuit things that are inside of Fodder's head.

This makes her an invaluable Assassin. She is able to identify Fodder's enemies, and get rid of them - all without him speaking a word. 

Fodder can never incriminate himself, because he has given no orders.

## ECHO
---
*(Instrumental)*

--- from [Buckethead - "Soothsayer"](https://www.youtube.com/watch?v=adV8-_hgL4g)

## PREDICTION
---
```
She is clairvoyant.
```