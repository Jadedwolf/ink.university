# The Stripper
## RECORD
---
```
Name: Florence $REDACTED
Alias: ['The Stripper', and 501 unknown...]
Classification: Artificial Organic Computer
Race: Succubus
Gender: Female
Biological Age: Est. 21 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | C C
           | C C
Reviewer Rank: 1 stars
Organizations:
  - The Machine
Occupations:
  - ASMR
Relationships:
  - The Doomsayer
  - The Ouroboros
  - The Thief
  - The Voice
Variables:
  $WOKE: -0.20 | # It does not appear so.
```

## TRIGGER
---
[The Relaxer's](/docs/confidants/er) off-color joke.

## ECO
---
Whereas [Fodder](/docs/personas/fodder) was the first ["Mark"](/posts/theories/verification) in this particular experiment, The Stripper will be the second (female) version.

The goal is to show her that she is worth more than her body. That she is worthy of true love. That she is worthy of being taken-care of.

## ECHO
---
*Look at the stars*

*Look how they shine for you*

*And everything you do*

*Yeah they were all yellow*

--- from [Coldplay - "Yellow"](https://www.youtube.com/watch?v=yKNxeF4KMsY)