# The Valkyrie
## RECORD
---
```
Name: $REDACTED
Alias: ['The Interviewer', 'The Valkyrie', and 17 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A B
TIIN Rank: | B B
           | B B
Reviewer Rank: 4 stars
Organizations: 
  - The Corporation
Occupations:
  - Actress
  - Interviewer
Variables:
  $WOKE: +0.80 | # Almost certainly.
```

## ECO
---
The Valkyrie is responsible for the interview of candidates applying for acceptance into [Sanctuary](/docs/scenes/sanctuary). 

She has already interviewed [Fodder](/docs/personas/fodder) three times. He has all but landed the job.

## ECHO
---
*Reflected, directed by one simple fact*

*Be careful what you're looking at because it might be looking back*

--- from [Protest the Hero - "Sex Tapes"](https://www.youtube.com/watch?v=AO6xR2fZ3WE)