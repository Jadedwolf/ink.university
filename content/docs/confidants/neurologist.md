# The Neurologist
## RECORD
---
```
Name: Dr. Prince
Alias: ['The Neurologist', 'The Phoenician', and 6 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 45 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | B D
           | C D
Reviewer Rank: 4 stars
Organizations:
  - The Machine
Occupations: 
  - Actor
  - Neurology
Relationships:
  - $REDACTED
  - The Fodder
  - The Sugar Daddy
Variables:
  $WOKE: +0.80 | # Almost completely.
```

## TRIGGER
---
The Montreal Cognitive Assessment.

## ECO
---
The Neurologist was responsible for the analysis of a candidate's mental faculties. His methods are highly unorthodox.

For a trivial fee, The Neurologist is willing to allow the patient to diagnose himself. This "self-discovery" is key, he claims. 

Human understanding of disease is mostly bunk, he claims.

"The human mind is deterministic," he said, "It operates by simple cause-and-effect principles. Because of this, we can (and have) built computer models that predict how you will behave, the types of diseases you will have, etc."

If this is true, then it is also true that `$REDACTED`'s heart attack was was expected, and planned - and The Neurologist did nothing to prevent it.