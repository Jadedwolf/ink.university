# The Tradesman
## RECORD
---
```
Name: James Grey
Alias: ['Bigfoot', 'Dad', 'Dexter', 'Father', 'Fred', 'Jim $REDACTED', 'Jim Carrey', 'Jim Newstead', 'Jim-1', 'Jim-2', 'Jim-3', 'Jim-4', 'Lucifer', 'Mr. Krabs', 'S8N MAVIS', 'SCP-5', 'Satan', 'The All-Father', 'The Commander', 'The Devil', 'The Surgeon', 'The Tradesman', and 1,424 unknown...]
Classification: Artificial Identity
Race: Archon
Gender: Male
Biological Age: 56 Earth Years
Chronological Age: 34 Earth Years
SCAN Rank: | D A
           | A F
TIIN Rank: | F A
           | A F
Reviewer Rank: 4 stars
Maturation Date: 10/17/2020
Organizations: 
  - Comet Ping Pong Pizza
  - The Church of Satan
Occupations:
  - Baker
  - Forklift sales
  - Inventor
  - Program Director
  - Surgeon
Relationships:
  - The Agent
  - The Con Man
  - The Cop
  - The Fodder
  - The Hope
  - The Jen
  - The Lion
  - The Marshall
  - The Monstrosity
  - The Negro
  - The Orchid
  - The Queen
  - The Reverend
  - The Scientist
Variables:
  $AVOIDANT:       -0.80 | # Avoids difficult situations as a coping mechanism.
  $FATHER:         +1.00 | # Definitely father.
  $IMMUTABLE:      -1.95 | # Almost completely. We suspect it's an act.
  $MENTAL_ILLNESS: -0.40 | # Does not show it. We suspect it's there.
  $WOKE:           +0.30 | # A little bit. A lot is being hidden from him.
```

## TRIGGER
---
*We are escalator walkers*

*in the brand-new temple*

*Came to reshape identities*

*Shed our skins*

*Be reborn and feel the same*

*Feel the same: that no one here is real*

--- from [Riverside - "Escalator Shrine"](https://www.youtube.com/watch?v=PYZV3e0PCWI)

## RESOURCES
---
[The Tradesman's fake resume](/static/reference/surgeon_resume.pdf)

## ECO
---
[Fodder's](/docs/personas/fodder) father was a good man. He worked hard, cared for his family, and would go out-of-the-way to help anyone. The man had empathy.

But he couldn't admit it, so completely had the virus taken hold of his mind. Nothing is real. Everything is fake. Everyone is lying. You can't trust the media, the government, corporations, or even your church. Everyone is corrupt. 

Family first. Trust yourself second.

Fodder could understand that perspective. But he could not abide by it. He wasn't going to leave his father behind.

The man was the reason for this entire thing. He knew all along. And he lasted long enough to see the end.

I was going to give that to him.

## ECHO
---
*Five soldiers forever sedated with the,*

*"No one's responsible" psychological drama of our social justice dribble*

*Her tiny steps tell lies about the choice I have to make*

*Resurrect a static lifetime starve to death my own mistakes*

*Pull the screaming trigger and watch your carcass bleed me dry*

*Or drop the gun and try to shake away the blindfold from your eyes?*

*Drop the gun, Drop the gun,*

*Drop the gun, Drop the gun!*

--- from [Protest the Hero - "Blindfolds Aside"](https://www.youtube.com/watch?v=CRsxzJpzros)

## PREDICTION
---
```
A nose flick. A chin touch. An awkward attempt to exit the conversation. They were all signals.

He can't talk.
```