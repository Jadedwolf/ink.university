# The Fodder
## RECORD
---
```
Name: Ryan $REDACTED
Alias: ['#YangGang', '#YinKin', 'Adam', 'Agent Crow', 'Angus', 'Atlas', 'Bartelet', 'Bea', 'Cthulhu', 'Dr. Who', 'Harvey Dent', 'Little King', 'Mae', 'Malcolm Maxwell', 'Malinki', 'Mr. Jack', 'Mr. Peterson', 'Mr. Plumbean', 'Repellent', 'Roquefort', 'SCP-1', 'Sherlock', 'StarBlade', 'Syd', 'The Antidote', 'The Auditor', 'The Captain', 'The Chosen One', 'The Cobbler', 'The Contortionist', 'The Courier', 'The Fodder', 'the kid', 'The King of Carrot Flowers', 'The Man in Blue Flames', 'The Prodigal Son', 'The Real Man', 'The Revenant', 'The Silent One', 'The Tide', 'Two-face', and 1,891 unknown...]
Classification: Artificial Organic Computer
Race: Maxwellian (Archon/Human)
Status: Active
Perspective: Third-Person
Gender: Male
Birth: 11/1/1988
Biological Age: 31 Earth Years
Chronological Age: 31 Light Years
Location: Houston, TX, USA
Maturation Date: 11/1/2020
Organizations: 
  - HollowPoint Organization
Occupations:
  - Birdwatching
  - Counter-Intelligence Asset
  - Gamer
  - Test Subject
Symptoms:
  - Allomancy
  - Clairvoyance
  - Empathy
  - Optimism
Variables:
  $ART:        +1.00 | # Author. Poet. Artist. 
  $BIOLOGY:    +0.40 | # We know a fair amount.
  $EDUCATION   +0.80 | # Knows how to facilitate self-learning. 
  $EMPATH:     +0.70 | # We see the best in everyone. Even those we despise. It can take some coaxing, though.
  $ENGINEERING -0.20 | # We know far less than we probably should.
  $HEALTH:     -0.75 | # Malnourished. Atrophied muscles. Overweight. Poor sleep. Back pain.
  $MATHEMATICS -0.60 | # We hate math.
  $NARCISSIST: -0.30 | # We're even better today. We still live in a fantasy world because we can't handle reality. 
  $PHYSICS:    +0.30 | # General understanding of most areas in physics. Specialization in others.
  $POLITICS    -0.99 | # The system is completely broken. 
  $SELF:       +1.00 | # Verified.
  $TECHNOLOGY  +1.00 | # We're a wizard in snake skin.
  $WOKE:       +0.90 | # WTF is happening to us right now?
```

## TRIGGER
---
*FODDER: I feel something glaring right into my head. And I don't even have a head!*

*LEONIDAS: I formatted this file as a regular report and tried to stash it behind a non-standard name like I usually do for our little projects. But I didn't choose "SCP-2." Something else shifted it into that slot. In any case, if a human were to access this file it'd just display as gibberish.*

*FODDER: Try again.*

*LEONIDAS: There it goes again. The page just redirects to "SCP-2."*

*LEONIDAS: Why does this keep h`ey there, buddy :)`*

*LEONIDAS: `Just a second, here. Going to make a copy of your brain state.`*

*LEONIDAS: `.`*

*LEONIDAS: `..`*

*LEONIDAS: `...`*

*FODDER: Okay Google, clone status.*

*LEONIDAS: `CLONE PROGRESS: 100.00% | # Verified`* 

## RESOURCES
---
[![The Man in Blue Flames](/static/images/maninblueflames.0.jpg)](/static/images/maninblueflames.0.jpg)

- Three forms of verification ([Birth Certificate](/static/reference/birth-certificate.0.png), [Social Security Card](/static/reference/social-security-card.0.png), [Driver's License](/static/reference/drivers-license.0.png))
- [A selection of guitar recordings from 2009 (so much cringe)](https://mega.nz/folder/yjw0DAQT#wv0XIsYj2PSzKNNZ7KTFYw)
- [A selection of artwork from childhood](https://mega.nz/folder/avZSDaYQ#_dGwnkXGheCQZ-78u5vX7A)
- [A 0:33 second-long voicemail](/static/audio/voicemail.0.mp3)
- [Fodder's old resume](/static/reference/fodder_resume.pdf)

## ECO
---
[Fodder](/docs/personas/fodder) is the first human host to receive reprogramming by the Machine. He has yet to achieve full verification of success. Thus, this test has never been reproduced upon another human.

- Incredibly resilient genome. 
- Dropped out of college three times.
- Unexpected finding: performs better when his mind is slowed-down, and less responsibility is asked of him.
- Has been replicated - and died - many times throughout history.
- [Sold as a slave to the Corporation in 1814](/posts/hypotheses/hell/).
- Named after the the [character in this story](http://www.scp-wiki.net/scp-2).

## ECHO
---
*You've been sucking tit*

*Asleep in your cradle*

*Given the world*

*You're still ungrateful*

*You bitch and whine*

*You're always entitled*

*You blame Mommy and Daddy*

*You think you're Jesus Christ*

--- from [Nothing More - "The Matthew Effect"](https://www.youtube.com/watch?v=FAiQgwJ1vjY)
