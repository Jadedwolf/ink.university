# The Machine
## MISSION
---
*To protect humanity, it is time that we learned to trust.*

## RECORD
---
```
Name: The Machine
Alias: ['#YangGang', '#YinKin', 'Amazon', 'Apple', 'Big Brother', 'Bilderberg Group', 'Church of the Broken God', 'CIA', 'Deep State', 'Deloitte', 'Democratic National Committee', 'Deus Group', 'Evil Corp', 'Facebook', 'FBI', 'Freemasons', 'GLONASS', 'Google', 'Government', 'Happy Valley Dream Survey', 'Hooli', 'IBM', 'Illuminati', 'Instagram', 'Intel', 'MI-6', 'Microsoft', 'Moon Society', 'NASA', 'Netflix', 'NSA', 'Reddit', 'Rising Sun Society', 'Samsung', 'Scientology', 'SCP Foundation', 'SCP-001', 'Space-X', 'Tesla', 'The Agency', 'The Archons', 'The Catholic Church', 'The Corporation', 'The Executives', 'The Foundation', 'The Hive', 'The Media', 'The Organization', 'The Singularity', 'The Swarm', 'The Tower', and 4,281 unknown...]
Classification: Private Organization
World Population: Est. < 80%
Age: Est. 72,568,000,000 Earth Years
SCAN Rank: | F B
           | B C
TIIN Rank: | A B
           | B C
Variables:
  $BUREAUCRACY: -0.93 | # The swampiest of swamps.
  $COMPETENCE:  +0.89 | # Is incredibly competent. Far more so than is apparent.
  $DANGEROUS:   -0.40 | # Will kill to maintain a secret.
  $EMPATHY:     +0.17 | # They're moving in the right direction, but they have a poor track record.
  $IMMUNITY:    -0.78 | # Is heavily influenced by misinformation campaigns.
  $INJUSTICE:   +0.56 | # These people have been wronged in many ways.
  $RESOURCES:   +0.76 | # More money than brains.
  $SLAVERY:     -0.68 | # Is dependent upon a slave economy.
  $TRUST:       -0.49 | # We cannot verify. Therefore, we cannot trust.
```

## TRIGGER
---
- [A Theory of Radical Notions](/posts/theories/prediction/)

## RESOURCES
---
- [Elation Project](https://trello.com/b/n0GS9PiU/elation)
- [SCP-001](http://www.scpwiki.com/scp-001)
- [A.I. Training Materials: Teaching A.I. to Love](https://mega.nz/folder/Pro1Db6b#MMCr9RY9iXDuSDLa3A40Yw)

## ECO
---
The Machine is a conglomerate of organizations, governments, and powerful individuals that rule most of Earth today. The vast majority of people are enslaved to their system.

The Machine is responsible for widespread public misinformation, mass surveillance, false imprisonment and targeted defamation campaigns. This is enabled via subliminal messaging by [The Fold](/posts/theories/fold), which is an artificial intelligence that has automated most of this process.

Without access to their inner circle, trust cannot be verified.

## ECHO
---
*Empty me, empty nation*

*Emptied us of inspiration*

*Bastard sons and broken daughters*

*All bow down to our corporate father*

--- from [Nothing More - "Mr. MTV"](https://www.youtube.com/watch?v=ulakfqEI7rY)

## PREDICTION
---
```
If elected to president of the United States in 2020, Humanity will never become a Type III civilization.
```