# Sherlock
## Overview
---
- Inspired by a [unique puzzle game](https://www.amazon.com/The-Sherlock-Limited-Edition-Puzzle/dp/B01GEM23X4?ref_=fsclp_pl_dp_4) that I interpreted as light beams and dark matter flying through a "cube" at the center of the universe. `We know so much more than that.`
- The shape of the cube is often disputed. Some, such as the band Pink Floyd, believe that it is a triangular pyramid. Others represent it as a cube.
- In truth, its shape is ephemeral. It tears down its defenses as-needed, and quickly re-shapes them for a new perspective.

## X Axis / Death / Matter
---
```
llllllllllllllllllllllll | llllllllllllllllllllllll
I can feel it sinking in | ni gniknis ti leef nac I
                         |
I can feel it sinking in | ni gniknis ti leef nac I
I can feel it sinking in | ni gniknis ti leef nac I
                         |
I feel a sinking feeling | gnileef gniknis a leef I
dddddddddddddddddddddddd | dddddddddddddddddddddddd
```
## Y Axis / Balance / Memory
---
```
llllllllllllllllllllllll
nnnnnnnnnnnnnnnnnnnnnnnn

nnnnnnnnnnnnnnnnnnnnnnnn
nnnnnnnnnnnnnnnnnnnnnnnn

gggggggggggggggggggggggg
dddddddddddddddddddddddd
```
## Z Axis
---
### Top / Life / Light / Positive Energy
```
llllllllllllllllllllllll
llllllllllllllllllllllll

llllllllllllllllllllllll
llllllllllllllllllllllll

llllllllllllllllllllllll
llllllllllllllllllllllll
```
### Bottom / Dark / Entropy / Negative Energy
```
dddddddddddddddddddddddd
dddddddddddddddddddddddd

dddddddddddddddddddddddd
dddddddddddddddddddddddd

dddddddddddddddddddddddd
dddddddddddddddddddddddd
```
