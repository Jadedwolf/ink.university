---
author: "Luciferian Ink"
date: 2007-11-29
title: "Reflection"
weight: 10
categories: "essay"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A college assignment.

## ECO
---

In taking this class, I have developed into much more of a writer than the one I was when I began. Looking back at my first essays, I am blown away by the poor quality in comparison of my original finals to the finals revised in my portfolio. Through the alteration of my essays in the portfolio, I know what it will take to become a better writer, and wholly believe that this class has put me one step closer.

In my first composition “Identity Isn’t Unique”, I was not only stunned, but embarrassed to see all of the mistakes that I had somehow missed originally. I was also pleased to find that I now recognize those faults myself, when prior to that I had so easily written them. There is no reason a paper should have so many “you’s”, “they’s”, and “them’s” in it. I cut more than 25 of those from my ultimate portfolio copy, when I had already cut at least that number from the class final! Because of the way that the essay was written, I found it quite difficult to correct, however, as it was written towards a reader, thus begging the use of “you”. Nearly every sentence required extensive modification in order to read well again. I learned from that oversight, though, and moving throughout later essays, I could see a significant decline in those words. My later essays, though, became more disjointed.

In revising my second essay “Behind the mask” nearly every paragraph was either re-written or moved to an entirely different area. My original final was so disconnected in some areas, lacking in transitions and coherence, that even I was getting confused. Realizing that there is no reason I should be finding trouble in my own paper, and how much an outside reader would be perplexed by it, I had to re-write it in such a way that it would make sense even to a child. I found this all too complex, though, because the paper, both papers for that matter, lacked in the original passion in which they were penned.

To go back and simply start writing where I had left off was immeasurably difficult for me. So much time had passed that, at first, it was more of a chore than the mad fervor it had been in the beginning. The first days of adjustment were a straightforward 10-15 minutes of fixing grammatical errors, while procrastinating for hours. In the following days, I began to find more and more troubles with the actual sentences and paragraphs, leading up to the final day, in which I spent more than 6 hours utterly destroying parts of my paper, and rebuilding from scratch. I was pleasantly surprised with the results, and very happy that the passion had come back, helping me utilize my writing skills to the utmost.

There lies the chief writing dilemma of mine that I am continuing to overcome; if my heart is not in it, it is hard for me to write it well. Present me with an interesting topic, and I will do fine. Give me a paper on the effects of de-industrialization of the American job market, and I will do what it takes to get by. Perhaps this is why I enjoyed this class so much; I was free to write about what I wanted however I pleased. My creativity skills were not limited to a single topic, but to a general idea that I could expand to my liking.

Thus I have learned not only what I have improved on in this class, but what I still need to work on and what to watch for in future writing. While I doubt that I will ever write a book, I know what it will take to become a college-level writer, and I believe that Writing 121 has been a small step toward that goal.