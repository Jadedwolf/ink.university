---
author: "Luciferian Ink"
date: 2020-07-15
title: "The Hospital"
weight: 10
categories: "audit"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Hospital
Type: Emergency Audit
Auditor: Malcolm Maxwell
Date: July 2020
Result: C
```

## RESULT
---
### Synopsis
The Hospital is a medical facility located in Houston, TX. [Malcolm](/docs/personas/fodder) spent 3 days here before being discharged.

### Context
Malcolm was admitted to the hospital with terrible stomach pain. After MRI, it was found that he was in the process of passing three kidney stones. He was sent home after Lithotripsy, which broke the stones into pieces he could safely pass.

### Architecture
The facility was rather old and dated, but serviceable. 

### Processes

- Everything was clean and functional. Nothing to complain about here.
- Blood was drawn at 3am in the morning. Annoying, but tolerable. 
- Technicians are unable to take vital signs properly. The heart rate monitor is placed onto the same hand as the blood pressure monitor - invalidating the test.
- He had to travel by ambulance twice, racking-up costs.
- The Lithotripsy machine did not exist at our hospital. So, we had to spend 2 days in pain, waiting for it to return. No doubt, the hospital doesn't have one because some company, somewhere, is making a profit off of traveling around to different hospitals with this machine - instead of just selling one to the hospital directly.

### Costs

- We did not have insurance, and had no idea how we would pay the cost of treatment.
- Upon first meeting the Urologist, he would immediately say, "You're going to pay me, right? I'm not going to do this procedure unless you pay me first." Of course, we knew that he couldn't legally refuse treatment.
- He would go on to tell us that it would cost $1200 for this 15-minute procedure (not including hospital fees, ambulance costs, etc.) He was giving us a deal; normally, it would cost $3000. Of course, we knew that this was bullshit. We've worked in healthcare. If he were go through an insurance company, he'd be making even less than $1200 for the procedure. He was milking us for more money than he'd normally get.
- Upon transport to the operating room, we were never presented with a bill. 
- After completion of the procedure, the Urologist exclaimed, "Why didn't you pay me like I asked? I never would have done this if I had known you wouldn't pay."
- One month later, and we have still not been billed.

## Conclusion
---
Overall, the medical system wasn't a horrible experience. There are things to be fixed, for sure, but quite a lot seems to work.

## PREDICTION
---
```
Somebody already paid that bill. Or, the Urologist is doing us a favor. 
```