---
author: "Luciferian Ink"
date: 2019-12-08
title: "The Medicine Wears Off"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*See what I get*

*When the medicine wears off*

*You're moving fast*

*Try a little, see if I can make*

*The moment last*

*You'll burn the night up like a star*

*You'll break this heart, you know*

*When you're coming 'round, you're*

*Taken by wonder of it all*

*Where are you now?*

*Longing for the state of mind*

*Oh, I remember why we started blocking all we said*

--- from [Karnivool - "The Medicine Wears Off"](https://www.youtube.com/watch?v=A6t_hKN0WVg)

## CAT
---
```
data.stats.symptoms [
  - mourning
  - determination
]
```

## ECHO
---
*I think there's a pulse*

*But I don't remember feeling*

*Anything close to this*

*And I don't know if it's worthwhile*

*But I hope so*

*'Cause I don't feel so well*

*No I don't feel so well*

*I've gotta keep a grip on this*

*But the rising tide could still*

*Pull us underneath*

*And leave us to the ocean*

*Either way*

*I'm starting to feel like something's wrong with this poison*

*'Cause in my veins it's burning*

*And I hope you hold a place for us*

*Far enough away*

*From all the flames they like to tell us burn*

*And I hope you hold a way for us*

*But I don't really know you*

*I don't really know you...*

*Why the hell did I seek the truth?*

*Of all I see in its reflection*

*But part of me regrets it*

*And I just wanted to see*

*Now it's clear*

*We're alone in this*

*It's your funeral, it's your dying day*

*So make amends*

*For the end will leave us nothing*

*It's your funeral, it's your dying day*

*So make amends, one last stand*

*Then leave with nothing!*

*When I breathe again,*

*Will my lungs fill with fire?*

*When I breathe again, I hope it's ok*

*Will I see again?*

*When the smoke clears, who will still remain?*

*When I see again, I hope it's ok...*

*Chemical fires will signal we're dead and gone*

--- from [Karnivool - "Aeons"](https://www.youtube.com/watch?v=2TKC3jW6fzk)