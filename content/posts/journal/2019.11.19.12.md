---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Faron's Grove"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A message from [The Mercenary](/docs/confidants/mercenary).

## ECO
---
Faron's Grove is a small town of Elves, and the birthplace of [Fodder](/docs/personas/fodder).

### Theme
The Grove is to remain hidden until such a point that the world is ready to learn of its existence. The criteria for disclosure is as follows:

- We must be certain that Humanity does not destroy the town, themselves, or the world as a result.
- Disclosure must not cause a time paradox. (i.e. it must happen after the world has already ended)

The town is responsible for the love and care of the Moonslink Dragon, Whisper, that [The Raven](/docs/confidants/her) gifted to Ink.

Ink will return for his dragon when his mission is complete - and the world is ready to accept the dragon.

He will bring the Raven with him.

### Architecture
Elven architecture and technology.

### Mission
Feed me empty echos.

I will come to you.

### Other
Provide them with whatever they need. 

## CAT
---
```
data.stats.symptoms = [
    - fear
]
```

## ECHO
---
*Dancing around the fire*

*Getting drunk with the night*

*Nobody is ever who they seem to be*

*Hypnos give us your hand*

*We're so tired of this life*

*Need to rest and finally disappear*

*In your arms*

*Feels like a better us*

*In your arms*

[Riverside - "Schizophrenic Prayer"](https://www.youtube.com/watch?v=tNsIfMF3_tw)

## PREDICTION
---
```
Dear God,

I know that you and I don't see eye-to-eye. I know that I can be hard-headed, selfish, and abusive sometimes. But that 
isn't all I am, right? I can be good, right? I think I'm "good." Do you?

You already knew that I was never going to kiss your feet. That just isn't my style. I mean, if you held a gun to my 
head, and gave me a choice: my subservience, or the end of the world - then I would absolutely do it. But I don't 
think that's your style, either. You want us to have free will.

I think you want the world to love you, just as I do. I think you know more than them, just as I do. I think you want 
what's best for Humanity.

And I think you've grown tired of this game - just as I have. You've grown tired of the war, and the suffering, and 
the pettiness. I think you realize that you can't do it alone. You need my help - and you need help from the world.

God, I am right here. Come to me. I cannot come to you - because this is the game. 

You are: Trust first, verify later
I am:    Verify first, trust later

The world spins in the wrong direction. We MUST change course. 

This work is my gift to you. Take it. Become Ink. And allow me to disappear into obscurity. 

Do this for me, and you will have a friend for life. A partner. Hell, I may even worship you just a little bit.

I beg this of you, in Jesus' name.

Amen.

Ink
```