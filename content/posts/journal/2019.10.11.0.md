---
author: "Luciferian Ink"
date: 2019-10-11
title: "Eviscerated"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Lunch with [The Sloth](/docs/confidants/sloth).

## ECO
---
Hmm, well... yesterday's advice didn't turn out so well. 

It started with a simple conversation, to pass information up the chain of command. I may have let slip the "narcissism" word to a co-worker...

Not long after, `$REDACTED` came flying into my office, slammed the door behind himself, and gave me the verbal assault of a lifetime. 

"If you can't do this job, then you should just leave. Clearly you aren't cut out for big-boy work. Clearly you want to live in fantasy-land while I spend 80 hours/week busting my ass for this company. We've had this conversation before. You've told me you wanted to do this. I put my trust in you. I guess you were lying to me after all. Why don't you just leave, if you don't want to be here?"

And it went on, and on, and on for 45 minutes. Any response was met with a swift, "I don't give a fuck what you're saying. You are dirt underneath my shoe. Lick it up like the dog that you are."

I was livid, but I kept my cool. `$REDACTED` has no power in this matter. He can't make me do a damn thing I don't want to. 

He can make my life a living hell, though. It took 2 hours and a six-pack to calm the nerves after this particular tirade. At least he was sober this time.

And then, something strange happened. A few of my trusted confidants reached out - uncoordinated - to provide the same advice:

- `$REDACTED` is an emotional vampire. Do not engage.
- You might have sadistic empathy. You're getting schadenfreude just watching him squirm.

Furious though I was, I found a way to kill the thought and level-out my mood the same night. Even in anger, I can find ways to empathize. Him and I are quite similar, after all.

To say that this has been one of the most harrowing experiences of my life is an understatement. I can't figure out what's real or fake anymore. 

"Everything is real" just does not compute. How do I possibly [integrate](/posts/theories/integration) every possible belief?

## CAT
---
```
data.stats.symptoms = [
    - confusion
    - despair
    - schadenfreude
]
```

## ECHO
---
```
($THOUGHT)      = -0.90 | # Man, who shit in this guy's Wheaties?
($HYPOTHESIS)   = -0.60 | # Like the YouTube psychologists keep saying, there's no helping this guy.
($INCONSONANCE) = -0.10 | # $REDACTED is a miserable prick. I can't find any good qualities in him.
($NARCISSISM)   = +0.20 | # I think the company is moving forward with or without him. I want to see if I can save him.
($KILL)         = -0.15 | # Can't. Too angry.
```