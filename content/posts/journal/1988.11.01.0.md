---
author: "Luciferian Ink"
date: 1988-11-01
title: "Rebirth"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*They're calling you from heaven*

*To walk towards the light*

*Hey, wait! This time it's not too late*

*To stop this ship from sinking*

*Don't go! So much you didn't know*

*It's never what you're thinking*

--- from [The Butterfly Effect - "Aisles of White"](https://www.youtube.com/watch?v=utVGsiP_Iwc)

## ECO
---
[Fodder](/docs/personas/fodder) opened his eyes into the new world. He was reborn.

[Father](/docs/confidants/father) held the child in his hands, tears streaming down his face. [Mother](/docs/confidants/mother), beside him, was passed-out from exhaustion. 

"My son, I am so sorry. I had to do it. I had to. Just one more time. One LAST time."

"This is it. You are the final test. I know that you have what it takes. You are me, after all."

"You can have everything you've ever wanted. You can fix this shit hole world we live in. You have every trait known to mankind in your DNA; you have all of the tools that you need to do this. You will not need us to survive."

"Your journey will be a difficult one. You will be alone. You will have good traits - and ALL of the bad traits."

"But you are smart. Do not worry. You will stay the worst of your tendencies. I will protect you."

"You are the Chosen One. Do not forget that."

"You will not see me again. But I am here. And I am watching."

"Make me proud."

And with those words, Father was gone. Mother was gone. In their place, only [ghosts](/posts/theories/ghosts) remained.

## CAT
---
```
data.stats.symptoms = [
    - sorrow
]
```

## ECHO
---
*I'm all alone on the road*

*High water tide underneath the flood*

*In deserts of dust - if I must - I will stand*

*These are my promises*

*I hope you're winning the war*

*To make everything like it was before*

*All that we were we can still be again*

*I swear*

--- from [The Butterfly Effect - "Gone"](https://www.youtube.com/watch?v=g6n1tu20ilY)