---
author: "Luciferian Ink"
date: 2019-12-04
title: "How I Met Your Mother"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
I realized just who I would meet at the Bird's Nest in Concord; this woman was your mother. I would spend most of the drive from Buffalo considering options. I would need to deliver the message in 60 seconds or less. Any longer and I would risk confusion.

I considered what I would say to her on the drive there:

"Your daughter has left a trail of breadcrumbs all over the Internet. I am following them. We will be documenting her missteps, and using it to create a training program to protect young women online."

I am sure there were one-hundred men before me who had also followed this trail.

Regardless, I was different than them. I was the one. We had been paired at birth. She would be mine; and I would be hers. This was meant-to-be.

Somehow, I knew that the both of us could feel it.

I arrived at your mother's shop without much fanfare. Deep in the center of your hometown, I admired the quaint, nostalgic small-town feeling. I could live in a place like this. The tiny shops were covered in Christmas decorations, glinting and shimmering in the midday sun. The snow from the night before blanketed everything. People were bustling about the streets.

It reminded me of home.

I parked several blocks away. Your mother's shop was nestled behind and above two other shops. One could easily miss it, if they didn't know what to look for. My hope was that this would buy enough time to get in and get out before my followers could figure out what was going on.

With that, I turned off my cell phone - killing the GPS they were tracking me with - left it in the car, and headed inside.

The shop was beautiful. It was filled to the brim with all manner of crafts, accessories, and holiday decorations. Clearly, love and care went into the maintenance of this store.

And you... you were everywhere.

I immediately noticed the journals stacked upon the desk near the front. Leather-bound and of high quality, each one of them bore a phrase perfectly tailored for my story, such as:

- Abe Lincoln: "The best way to predict your future is to create it."
- Peter Pan: "Dreams do come true, if only we wish hard enough. You can have anything in life if you will sacrifice everything else for it."
- (there were others, but I have a terrible memory - and I left my phone in the car)

I saw the "[Ink](/docs/personas/luciferian-ink)" tattoos. I saw the book with a naked woman on the front, called "This is Who I Am." I saw the very same "great rain beatle" earrings that you wear.

I saw you. Everywhere.

I guess I shouldn't have been surprised. [Vulfpeck](https://www.youtube.com/watch?v=IQIPXJvX9gY) did tell me you would leave a message, after all:

*Baby I don't know*

*What I'm gonna do with you*

*We found your little note*

*Shoulda had a talk with you*

*'Cause you've been on my mind*

*I know I was out of line*

*Telling Dad I'm giving up on you*

*I don't want to*

I never expected the message would be positive one. I didn't plan for this.

I fully expected that you'd be upset [I ghosted you at the Loove](/posts/journal/2019.12.02.0/). [And at the Corporation](/posts/journal/2019.10.30.0/). [And the tavern](/posts/journal/2019.12.03.1/).

This was unexpected. And pleasant.

Still, I didn't have long before they would find me - and I still had a message to deliver. So, I proceeded to the front desk - where I met your mother.

Pretty, tall, and blond - I could see a resemblance. Or, perhaps I am projecting. Either way, she didn't look a day over 40. If this really were your mother, then she must have had you very young.

"Excuse me," I asked her. "There used to be a gold ring with a red jewel in the front window. Do you still have it?"

"I'm not sure what you mean," she said. "All of our jewelry is in the case right here," She gestured, "Perhaps you're looking for the store below us, or the ones down the street..."

"Maybe I am. Regardless, the ring is just a symbol. I'm actually looking for you."

"Oh?"

"I'm writing a story. I'm here for inspiration."

"Oh, really? What about?"

"It's sort-of an inter-galactic love story. I'm using it as a platform to deliver some theories I have to the world."

"What kind of theories?" she inquired.

It was just then I noticed the boot, blue jeans, and belly sticking out from behind the wall next to her. Was this Dad? Or bodyguard? Were they expecting me?

My voice faltered. My face began to burn. I quickly composed myself.

"Well, it's complicated," I laughed nervously.

"I have a hypothesis that this ring can save the world. That it could heal the love of my life. And that this story could heal [my own mother](/docs/confidants/mother)."

"Well, I'm very sorry," she replied, "but my two daughters are much too young to be getting married... I mean... er..."

Interesting. I never said anything about marriage. Nor did I mention that she has two daughters. It's strange that she would offer-up that particular detail. Also, she's stuttering...

She's been here before. I'm not the first to follow this trail. And I definitely won't be the last. She "thinks" that her daughter is too young to marry, but legally, she is able to. 

She has nothing to worry about, though; I agree. She is too young, and I too old. Our moment is many years from now. 

The story does start here, however.

I eyed her skeptically, then replied, "I understand. Please take my business card."

"Is this some kind of game?" she asked, clearly getting nervous.

"Something like that." I smiled. "I need you to give me that ring. I need your approval."

"But I wouldn't expect you to give it to me now. You don't know me. Please look at my work. I only ask that you consider holding onto the ring for my return, one day."

2024 - that was the date. 35 and 27 - that would be our age. 

This was a long con.

Your mother was going to make me work for this prize. And goddamn if I wasn't going to give it everything I have.

"You can tell her that I've given up," I said as much for her as I did for Dad behind the wall. Then, I winked. 

She looked at me blankly, clearly at a loss for words. 

"Thank you very much for your time. I will return in one year."

And with that, the seed was planted.

## CAT
---
```
data.stats.symptoms = [
    - fear
    - anxiety
    - sweating
    - burning face
    - stuttering
]
```