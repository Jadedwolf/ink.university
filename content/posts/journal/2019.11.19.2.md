---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: Arkham"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
The works of H.P. Lovecraft.

## ECO
---
Arkham is an established city located near the Massachusets coast. Some would argue that it is actually the town of Salem.

However, Arkham is ephemeral; its location is able to change. Thus, only one with the proper credentials may find it.

### Theme
Arkham is to follow the works of H.P. Lovecraft's Arkham as close as is practical. 

### Mission
Provide citizens with relaxation and/or pacification.

When rehabilitated, release them into their new home.

### Architecture
The central features of the city are to be Arkham Sanitarium and Miskatonic University. 

At the Sanitarium, patients are to be kept under induced-relaxation until they are rehabilitated.

At the University, students are to be taught about the ways of Cthulhu. They are to use his mind-controlling tentacles to lure-in more "patients."

At the top of the hill in the center of town resides a mansion, where students and professors alike live and work together.

### Other
Other areas should take inspiration from the first [Lonely Town](/docs/scenes/lonely-towns), GunsPoint.

## CAT
---
```
data.stats.symptoms = [
    - hope that I may give this gift to the Relaxer
]
```

## ECHO
---
*"That must be it!"*

*Through the summer rain of 1845*

*The coach had finally arrived*

*To the valley where the crossroads meet below*

*And where all darkness seems to grow*

*People blame it on the Hill*

*The hill where no one dares to go*

*The Mansion*

--- from [King Diamond - "Arrival"](https://www.youtube.com/watch?v=AwvVUT4MOhg)

## PREDICTION
---
```
The Relaxer will be required to make this town happen.
```