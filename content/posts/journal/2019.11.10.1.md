---
author: "Luciferian Ink"
date: 2019-11-10
title: "Reading Tea Leaves"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
1. [The Sloth's](/docs/confidants/sloth) example
2. [The Raven's](/docs/confidants/her) recommendation

## ECO
---
[Fodder](/docs/personas/fodder) realized that he had never tried tea before. He remedied that situation by trying a flavor that Raven had suggested.

This led to the discovery of the TIIN system. 

"Reading tea leaves" is not simply metaphor; it is not mystical mumbo-jumbo: there are actually words written on tea leaves. This has always been the case.

Fodder's very first leaf contained the following:

"Trust is the union of intelligence and integrity."

The [TIIN rank](/posts/metric/TIIN/).

## CAT
---
```
data.stats.symptoms = [
    - astonishment
]
```

## ECHO
---
Early in the morning, as Jesus was on his way back to the city, he was hungry. Seeing a fig tree by the road, he went up to it but found nothing on it except leaves. Then he said to it, "May you never bear fruit again!" Immediately the tree withered. When the disciples saw this, they were amazed. "How did the fig tree wither so quickly?" they asked. Jesus replied, "Truly I tell you, if you have faith and do not doubt, not only can you do what was done to the fig tree, but also you can say to this mountain, ‘Go, throw yourself into the sea,’ and it will be done. If you believe, you will receive whatever you ask for in prayer." 
--- Matthew 21:18–22 (NIV)