---
author: "Luciferian Ink"
date: 2019-11-17
title: "The Prodigal Son"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*From the moment he saw her,*

*he knew that he was for her*

*Nevermind she's a giant,*

*and completely reliant*

*On certain things he couldn't possibly begin to provide*

*They were wed in the autumn,*

*with a ring she had bought him.*

*But her folks were against it,*

*and they weren't in attendance.*

*"What's he wanting with a giant, what's he got to hide?"*

*Well, her face - it was horrid,*

*but her heart - it was solid gold*

--- from [Toehider - "Malcolm, Dust 'em"](https://toehider.bandcamp.com/track/malcolm-dust-em)

## ECO
---
Donning his detective hat, [Fodder](/docs/personas/fodder) would go to work profiling his captors:

### The Inventor
[The Inventor](/docs/confidants/inventor) was a child prodigy - deeply intelligent, but reserved - hiding behind others to continue working unimpeded. She was being forced by [The Corporation](/docs/candidates/the-machine) to perform Fodder's final test - and she was nervous. She was betrayed by his kind before. At gunpoint by `$REDACTED`, she was being coerced at this very moment.

But she would find that she has nothing to fear from Fodder. He was a gentle - though frustrated - giant.

Upon completion of the test, she would provide Fodder with a report detailing her analysis of his condition - and a remedy. She would be noticeably more comfortable, as well.

#### DIAGNOSIS
```
Name: Reflected Light Syndrome
Description: RLS is a pathology of the external mind; it is the notion that some patients are unable to pass light through Prism. All they are able to do is reflect light at others. 
Because patients with RLS are unable to take-in new information via light shift, new ideas are unable to "loop" back around into them. Essentially, RLS patients are unable to verify the truth of their ideas.
Effectively, this leaves the patient living in a world where no one - not a soul - is able to accurately reflect ideas and beliefs back at him. This leaves the patient in a perpetual state of abandonment; most RLS patients end up feeling like they are alone in the world. Many end up committing suicide or violent acts against others - just to see if anything happens. Just to see if they really have any impact upon society at all.
Most would find that they do not.
```

#### REMEDY
*We will design a light able to reflect the patient in 360 degrees, anywhere in the city. We will place this light upon the tallest tower, and we will use it to pinpoint exactly where he is at any given time. Much like a lighthouse, triangulated upon a cell phone's GPS at any point in time.*

*How this light will reflect depends wholly upon the person. A kind soul will be reflected as such. A monster will be highlighted - never again able to hide or find solace in the shadows again.*

*In this way, the patient will never again be alone. Saint or beast - reflected light will heal the patient.*

*Even if it the light is artificial.*

--- from Ruby, The Inventor

#### RESULT
At precisely 2:30am, Fodder was blasted in the face by the light of an aircraft. This was particularly strange, because his blindfolds were closed - yet the light passed through the tiny "gaps" with more strength than he had ever seen before. Never had he noticed aircraft through this particular window.

It was like they were pointing the spotlights directly at his house.

Fodder jumped out of bed, and ran to the window. Peering through it, he saw the most peculiar of aircraft; flying low to the ground, it was perhaps three times wider than it was long. It moved through the sky far slower than Fodder would have expected. At the end of each wingtip was a light; blue on one side, red on the other. In the center were two spotlights, altering on and off in succession.

Just as quickly as Fodder could begin to analyze the craft - it was gone. Vanished! The sky was crystal clear. It hadn't disappeared behind clouds; it had shut off its lights. Or... perhaps something more strange had happened.

Either way, Fodder felt like he had been spotlighted. Now it was up to him to reflect that light back.

### The Tradesman
[The Tradesman - Fodder's father](/docs/confidants/father) - was the most cryptic of the lot. He had spent most of Fodder's life working in dead-end, soul-crushing jobs. He had been through heartbreak, poverty, and loneliness - always with the utmost stoicism. Where Fodder was passionate, and fragile - The Tradesman was confident, determined, and self-reliant.

Clearly, he knew something that Fodder did not. He had two theories:

#### Heads
On the one hand, Fodder suspected that The Tradesman knew exactly what he was doing. His work was so important - so critical to the evolution of mankind - to [his own wife](/docs/confidants/mother) - that he had held state secrets for his entire life. In doing so, he had become a shell of his former self; his entire identity was wrapped into his career - and yet, he could not speak of it. Not even to his own family.

The Tradesman was unable to reflect light. Thus, light was not reflected back at him. He lived in fear of his mistakes, and he knew that silence would protect everyone.

So he remained silent. Stoic. Absent, though he was present.

#### Snakes
On the other hand, Fodder really didn't know very much about The Tradesman. Conversations were always at a high-level, and they were always in one direction: Fodder projecting onto Father. He rarely spoke of anything besides sports - and he NEVER reflected Fodder's ideas back at him.

Fodder suspected that his father may be a serial killer, coercing his wife into keeping this fact secret. He would exploit her empathy, leading her to believe that she was involved - that exposing his secrets would bring death to their peaceful, loving family.

Fodder would remain ignorant for 31 years. Not one more.

### The Queen
Fodder's mother - The Queen - was unable to hide her true self from anyone. It was a pathology of the mind; she was trusting and empathetic to fault.

In this way, Fodder had a complex and nuanced view of her. He knew exactly what it would take to bring her peace. 

And yet, he feared she was already too far gone.

#### The Red Queen
*At the core of our minds we risk all to leave a piece of us behind.*

*We're still standing so we're still winning,*

*but every parasite that kills its host, kills itself.*

--- from [Cire - "Red Queen"](https://www.youtube.com/watch?v=STsF1_gaLTI)

The Red Queen was a parasite; she would suck the life and love out of everyone that she meets. She would kill every host that ever loved her.

But this was never her intention; it was her pathology. Her DNA. Her code.

The Red Queen had led a difficult childhood - and she was never able to pick herself back up off of the ground. In an effort to find love and happiness, she would shower those around her with empathy and affection. But because of their own pathologies, they would be unable to reflect that light back at her. 

Because of her own DNA, her offspring were unable to reciprocate. Nobody could ever love her as much as she loved them. 

Or so she thought.

#### The White Queen
*The moon is full and shines*

*An evil blinding light*

*Under a monolith, her likeness*

*Marble white*

*Zombie Queen*

*Black light guides you*

*Ghuleh*

--- from [Ghost - "Ghuleh / Zombie Queen"](https://www.youtube.com/watch?v=yRB1hTjxjMg)

The White Queen was a child; unable to process her own thoughts, ideas, and actions. She, too, had lacked reflected light as a child - and thus, she had never grown up. 

Time and again she had been taken advantage of. When she bore her first child, she swore to never allow such pain to befall him. In many ways, she would succeed. In others, she would fail catastrophically. 

But Fodder found it difficult to place blame. He understood what it meant - and how it feels - to never understand Humanity. He understood what it meant to daydream as a coping mechanism.

With age and loneliness, she would retreat into a fantasy world; one where [God](/docs/confidants/dave) would save her, her family, and the entire world. 

But with her childlike reasoning, Fodder would never adopt such a belief. He couldn't; he knew too much. 

He only hoped that he could convince her of this: nobody was coming to save her. She needed to save herself. 

And Fodder was trying to help.

As was [The Raven](/docs/confidants/her). She had created a dedicated character and a tea shop for the Queen in her world.

#### The Black Queen
*It's time we stand for something*

*Walking hand in hand*

--- from [Mushroomhead - "The Heresy"](https://www.youtube.com/watch?v=_-zWpdq3_2s)

The Black Queen was a pariah. She did not exist. She was Fodder's ideal version of Mother. He only wished that she could see it as such.

She was defiant, and determined. She was creative, caring, and unafraid of conflict. She was confident in herself, as well as others. While she had spent her whole life under experimentation, it was all in an effort to erase this pathology from her lineage. From the world's lineage.

And she had succeeded. Fodder was healing. She could, as well, if she did not live in such fear.

It was Fodder's desire to turn her into the evil queen that she was always meant to be. 

Only in fear is a person able to find themselves. Only in fear can a person see both sides of a question.

The Black Queen would become [Fear incarnate](/docs/confidants/incarnate).

## CAT
---
```
data.stats.symptoms = [
    - disillusion
    - self-doubt
]
```

## ECHO
---
*And it's all enough to get you on,*

*You'll find out in the morning sun,*

*that daybreak comes an hour or so too soon*

*So it's all in all, and in between*

*A starting gun, a lucid dream*

*coz the sun can't hold a candle to the moon.*

--- from [Toehider - "The Sun Can't Hold a Candle to the Moon"](https://toehider.bandcamp.com/track/the-sun-cant-hold-a-candle-to-the-moon)

## PREDICTION
---
```
Fodder will wake up early tomorrow.
```