---
author: "Luciferian Ink"
date: 2019-11-17
title: "BREAKING NEWS: The Chosen Won"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
[Monica, The Adict](https://www.youtube.com/watch?v=Fa_OsnwgTAc)

## ECO
---
The trigger video and the article tell contrasting stories. Who is correct?

### Snakes
DETROIT (FOX 2) - A family is pleading for answers after a woman was killed in a hit-and-run crash on Detroit's southwest side back in May.

"I love her very much and I miss her and I need someone to please come forward for my child's justice. Please," said Helena Swenders.

The gut-wrenching pain is caused by the untimely loss of her daughter, Monica Swenders, who was killed by a hit-and-run driver near Fort and Calvary around 1:30 a.m. May 31.

"She got taken away way too early from the family," she said.

Swenders, who leaves a son behind, was hit by what police believe was a 2014-15 Ford Fusion that sustained left front-end damage. Those who called Monica A friend say this tragedy took away a kind spirit.

"I think that's why people loved her so much because she was unique, said Curt Langley, the victim's friend. "It was adorable just watching the way she would interact with people and just express herself.

Now the family is pleading for help in identifying who took their loved from them.

"Whoever could have possibly witnessed this please come forward, we need justice," said her cousin, Kristen Swenders.

Crime Stoppers is offering a $2,500 for any information leading to an arrest. Call 1-800-SPEAK-UP, and you can remain anonymous.

"Speak up please for our family," Kristen Swenders said.

### Heads
There exists a world where Monica is the Chosen One. But my world is not her world. She was too far-gone, and I too late to save her.

I remember a time when the world was carefree. I remember when I felt loved, and protected, and like I would always be cared-for. 

I remember going to the family cabin, in northern Michigan. I remember camping-out with the cousins, grilling hot dogs over the fire, playing "Bloody Murder" at night, riding jet-skis by day, and climbing the sand dunes by Lake Michigan. I had my problems as a child - but "up north" was not one of them. This is my best childhood memory.

As I grew older, things began to change. The family began to split apart. Family events became fewer and further between. I began to grow as a person - but I also began to self-destruct. I grew angry, and resentful towards the world. Towards [my mother](/docs/confidants/mother). Towards the stupid society we live in.

Over the course of 10 years, I lost myself. Anything could have happened. I could have become Monica. I still very well could.

Our world enables the destruction of lives. I refuse to believe that Monica could not be saved. She needed help. And the people in-charge aren't fucking creative enough to fix this problem.

When she needed help, she got a life on the streets. 

Even in my darkest hours, I know that this will never be my situation. I will always have a home to return to.

I am not the Chosen One. We all are.

And I am the Fortunate Son.

## CAT
---
```
data.stats.symptoms = [
    - gratitude
]
```

## ECHO
---
*It ain't me, it ain't me, I ain't no millionaire's son*

*It ain't me, it ain't me, I ain't no fortunate one*

--- from [Creedence Clearwater Revival - "Fortunate Son"](https://www.youtube.com/watch?v=ec0XKhAHR5I)

## PREDICTION
---
```
Monica is being cared for. She will be resurrected on the day the world ends.
```