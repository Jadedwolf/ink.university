---
author: "Luciferian Ink"
title: "The Garden Time Forgot"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
1. Dad.
2. [Vangough - "The Garden Time Forgot"](https://www.youtube.com/watch?v=Mr8NZ1U-CZQ)

## ECO
---

### Verse
*I think I've reached the end*

*A place where I can begin to feel alive again*

*Stones wrapped in green and white*

*Covered in broken light*

*Between the evergreen*

*Seeing this garden, seeing my past life has*

*Finally set my heart free*

### Chorus
*How did I end up in this place?*

*With the leaves against my face*

*Walking on stones among the dead*

*Seeing the words etched on the floor*

*What could they be there for?*

*Do they reveal the way back home?*

### Verse
*All my life I've sat and wasted away*

*Now it seems my dreams have led me astray*

*And I feel something is keeping me here*

*Can this be my dream is becoming real?*

*I can see so vividly the truth that I am the Rabbit King*

*Running free through mountain springs*

*I find the key to stay in this dream*

*I am him and he is me*

*Two souls bound deep inside this tree*

*Learning to breathe, walking to be*

*More than I was to serve this purpose*

*Turning to face all that I missed*

*Reaching for you, reaching for kingdoms!*

*Seeing this garden, seeing my past life has*

*Finally set my heart free*

*The door is closing quickly*

*I have to make up my mind*

*Do I stay or do I let go?*

### Chorus
*How did I end up in this place?*

*With the leaves against my face*

*Walking on stones among the dead*

*Seeing the words etched on the floor*

*What could they be there for?*

*Do they reveal the way back home?*

### Verse
*I can't believe I've come this far*

*It wouldn't be proper to leave this kingdom in distress*

*I see all of the leaves on the tree*

*On stone I can read all my history*

*Finding ways to live for today!*

*Break free of our fears*

*And take the time to hear my words*

*And never look back to the past*

*I beg of you my love*

*And break free of our dreams*

*And break free of our destiny*

*Look to the sky and you will find*

*The keys to life's mysteries*

*Turn to the sky!*

*Turn to the blue sky!*

*And I never thought I would say this last goodbye*

*To a life that I let just slip and pass me by*

*And it comes as no surprise to anyone*

*That I'd choose to be here among my wife and son*

*Now come and follow me*

*Into the Acorn Tree for one last ceremony*

*Show me love and show me mercy*

*For I am your best friend*

*Welcome to modernity where peace can reign again!*

*Yeah again!*

*Can you feel love tonight when we're together in here forever?*

*You lost your way but now I found you*

*And I promise to guide you*

## CAT
---
```
data.stats.symptoms = [
    - fear
    - sadness
    - remorse
    - understanding
    - gratitude
]
```

## ECHO
---
*Tommy used to work on the docks*

*Union's been on strike*

*He's down on his luck*

*It's tough, so tough*

*Gina works the diner all day*

*Working for her man*

*She brings home her pay*

*For love, for love*

*She says, "We've gotta hold on to what we've got"*

*"It doesn't make a difference if we make it or not"*

*"We've got each other and that's a lot"*

*"For love we'll give it a shot."*

--- from [Bon Jovi - "Livin' On a Prayer](https://www.youtube.com/watch?v=lDK9QqIzhwk)

## PREDICTION
---
```
We're halfway there. 
```