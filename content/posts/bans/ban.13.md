---
author: "Luciferian Ink"
date: 2020-08-01
title: "Soda"
weight: 10
categories: "ban"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
About 25lbs over the past 6 months.

## ECO
---
Soda is terrible for you. Employees of [The Corporation](/docs/candidates/the-machine) are forever and completely banned from drinking it.

## CAT
---
```
data.stats.symptoms [
    - certainty
]
```